This was the coding interview from SIE, I only put about 2 hours into it, but I *should* finish it out.

Parsing Metacritic Test
Write an application in which can do the following:
 1. Parse the HTML for the “Top Playstation 4 Games (By Metascore)” on Metacritic’s PS4 page: (http://www.metacritic.com/game/playstation-4). Expose a method which can return the parsed information as an array of JSON elements that looks like the following:


[

   {

       "title": "XCOM: Enemy Within",

      “score”: 88

   },

   {

       "title": "Assasin’s Creed IV: Black Flag",

      “score”: 88

   }

… etc ...

]


2.  Expose a REST API for retrieving top PS4 games. There should be 2 exposed methods:


A HTTP “GET” request a “/games” returns all top PS4 games on metacritic page

A HTTP “GET” request at “/games/TITLE_OF_GAME_GOES_HERE” returns JSON for a specific job that matches the corresponding job title. For example, an HTTP GET at /games/Gran%20Turismo%206 should return an individual JSON object for Gran Turismo 6 like so:

   {

       "title": "Gran Turismo 6",

      “score”: 81

   }


Deliverables:
A. Provide the source-code, which satisfies 1 and 2 described above.
B. Provide “Readme” style documentation on how to run your code.
C. Provide unit tests to prove the correctness of your code

