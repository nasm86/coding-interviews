#!/usr/bin/env python3

import cgi
import datetime
import json
import re
import requests
import sys
from bs4 import BeautifulSoup, SoupStrainer
from http.server import HTTPServer, BaseHTTPRequestHandler

class Siever(BaseHTTPRequestHandler):
    user_headers    = {'User-Agent': 'SIETest/1.0'}
    meta_url        = "https://www.metacritic.com/game/playstation-4"

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_HEAD(self):
        self._set_headers()

    def do_GET(self):
        pass

    def do_POST(self):
        (ctype, pdict) = cgi.parse_header(self.headers.get('content-type'))
        if ctype != 'application/json':
            self.send_response(400)
            self.end_headers()
            return

        clen    = int(self.headers.get('content-length'))
        jmsg    = json.loads(self.rfile.read(clen))
        headr   = self.headers
        tstamp  = datetime.datetime.now()
        self._set_headers()

    req     = requests.get(meta_url, headers=user_headers)
    bsoup   = BeautifulSoup(req.text, 'html.parser', parse_only=SoupStrainer("div", class_="browse_list_wrapper  browse-list-large"))
    test    = [ div.text for div in bsoup ]
    print(f"test {test}")

def run_server(server_class=HTTPServer, handler_class=Siever, addr="localhost", port=5151):
    server_address = (addr, port)
    httpd = server_class(server_address, handler_class)
    print(f"Starting server on {addr}:{port}")
    httpd.serve_forever()

if __name__ == '__main__':
    try:
        print("This is where we *would* start the server\n")
        #run_server()
    except KeyboardInterrupt:
        print("\n")
        print("Stopping server...")
        print("Thanks for using it, have a great day!\n")
        sys.exit(187)
