#!/usr/bin/env python3

import csv
import math

def get_leg_length():
    #dataset1 is mainly for leg length 
    with open("dataset1.csv", newline='') as ds1:
        dino    = {}
        data    = csv.reader(ds1, delimiter=',')
        for leg in data: 
            # hackish but we know NAME is in the header
            if 'NAME' in leg:
                pass
            else:
                dname       = leg[0]
                dlength     = leg[1]
                dino[dname] = float(dlength)
        return dino

def calc_dino_speed(dinodata):
    #dataset2 is stride and stance
    with open("dataset2.csv", newline='') as ds2:
        dcomp   = {}
        blain   = {}
        data    = csv.reader(ds2, delimiter=',')
        for dino in data:
            # hackish but we know name is in the header
            if 'NAME' in dino:
                pass
            elif 'bipedal' in dino: 
                dname           = dino[0]
                bstrd           = dino[1]
                dcomp[dname]    = float(bstrd)

        #munge all the datas
        for saur, strd in dcomp.items():
            for lzrd, llen in dinodata.items(): 
                if lzrd in saur:
                    stride          = float(strd)
                    dinoleg         = float(llen)
                    dinoname        = lzrd 
                    dspd            = ((stride / dinoleg) - 1) * math.sqrt(dinoleg * 9.8)
                    blain[dinoname] = dspd 

        #sort that shit
        speed = sorted(blain.items(), key=lambda s: s[1], reverse=True)

        #display that shit
        for dname, dspeed in speed:
            print(f"{dname} {round(dspeed, 2)}")

if __name__ == '__main__':
    try:
        dinodata = get_leg_length()
        calc_dino_speed(dinodata)
    finally:
            pass
