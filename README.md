
# Coding Interviews

This is where I'm going to dump all the coding interviews as I do them for future reference. 

Also using Phil's repo https://gitlab.com/phollenback/whiteboardofdoom/tree/master for reference, I feel fine as his is in Go, mine will be python (at least for the time being).
