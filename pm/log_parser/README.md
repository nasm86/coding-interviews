PostMates log parser take home, 1hr timed. 

Given a fairly standard access log, find the successful GET calls on images and add up all the bytes
transferred and sort them lexographically.

I wrote this in about 45 mins after I got home and relaxed, had a beer and wasn't buried under anxiety, failed the timed test miserably. But I learned a lot :).