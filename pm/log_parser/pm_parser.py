#!/usr/bin/python3

import math

#log file from http://www.almhuette-raith.at/apache-log/access.log

def convert_size(size):
   if (size == 0):
       return '0B'
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size,1024)))
   p = math.pow(1024,i)
   s = round(size/p,2)
   return '%s %s' % (s,size_name[i])

with open('access.log', 'r') as alog:
    files = {} 
    total = {} 
    for line in alog:
        if 'GET' and 'image' in line:
            if '200' in line:
                data    = line.split(" ")
                fname   = data[6]
                nbytes  = int(data[9])

                if fname in line:
                    files[fname] = nbytes

                for image, bytey in files.items():
                    if image in total:
                        total[image] += bytey
                    else:
                        total[image] = bytey

    skeys = sorted(total.items(), key=lambda x: x[1], reverse=True)
    for key, val in skeys:
        print(f"{key} {convert_size(val)}")
