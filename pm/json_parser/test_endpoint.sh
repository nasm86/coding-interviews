#to make the test consistent zero out the log files first (only for this test condition)
true > postmates.log

#test the endpoint
curl -X POST -H 'Content-Type: application/json' -d "{
  \"type\": \"delivery-started\",
  \"courier_id\": 1234,
  \"location\": {
    \"latitude\": 37.7749,
    \"longitude\": 122.4194
  }
}" localhost:5150

curl -X POST -H 'Content-Type: application/json' -d "{
  \"type\": \"delivery-ended\",
  \"courier_id\": 1234,
  \"location\": {
    \"latitude\": 37.7749,
    \"longitude\": 122.4194
  }
}" localhost:5150

#this should 400 with the wrong content type
curl -X POST -H 'Content-Type: text/html; charset=UTF-8' -d "{
  \"type\": \"delivery-started\",
  \"courier_id\": 4321,
  \"location\": {
    \"latitude\": 37.7749,
    \"longitude\": 122.4194
  }
}" localhost:5150

#more cannon fodder
curl -X POST -H 'Content-Type: application/json' -d "{
  \"type\": \"delivery-started\",
  \"courier_id\": 4321,
  \"location\": {
    \"latitude\": 32.7709,
    \"longitude\": -72.4194
  }
}" localhost:5150

curl -X POST -H 'Content-Type: application/json' -d "{
  \"type\": \"delivery-ended\",
  \"courier_id\": 1234,
  \"location\": {
    \"latitude\": 32.7709,
    \"longitude\": -72.4194
  }
}" localhost:5150
