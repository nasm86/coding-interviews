Start the server:

*  chmod +x start_server.sh
*  sh ./start_server.sh

Stop the server:
*  CTRL+C to stop the server

Log file:
postmates.log

Manual test (yes its an ugly blob):

`curl -X POST -H 'Content-Type: application/json' -d "{
  \"type\": \"delivery-ended\",
  \"courier_id\": 1234,
  \"location\": {
    \"latitude\": 37.7749,
    \"longitude\": 122.4194
  }
}" localhost:5150`

Automated tests:
*  chmod +x test_endpoint.sh
*  sh ./test_endpoint.sh
