#!/usr/bin/python3 

import cgi
import datetime 
import json
import sys
from http.server import HTTPServer, BaseHTTPRequestHandler

class Soiva(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
    
    def do_HEAD(self):
        self._set_headers()
        
    def do_POST(self):
        (ctype, pdict)  = cgi.parse_header(self.headers.get('content-type'))
        clen            = int(self.headers.get('content-length'))
        jmsg            = json.loads(self.rfile.read(clen))
        head            = self.headers
        tstamp          = datetime.datetime.now()
        self._set_headers()
        print(f"{head}")

        with open('postmates.log', 'a') as pm_log:
            if ctype != 'application/json':
                uagent  = self.headers.get('User-Agent')
                self.send_response(400)
                pm_log.write("\n")
                pm_log.write(f"BAD REQUEST: using '{uagent}' at '{tstamp}' bad content-type '{ctype}'\n")
                pm_log.flush()
                pm_log.close()
                self.end_headers()
                return
            else:
                pm_log.write(f"\n{tstamp}\n")
                json.dump(jmsg, pm_log, indent=2, sort_keys=False)
                pm_log.write("\n")
                pm_log.flush()
                pm_log.close()
                self.end_headers()
        
def run_server(server_class=HTTPServer, handler_class=Soiva, addr="localhost", port=5150):
    server_address = (addr, port)
    httpd = server_class(server_address, handler_class)
    print(f"Starting httpd on {addr}:{port}")
    httpd.serve_forever()

if __name__ == '__main__':
    try:
        run_server()
    except KeyboardInterrupt:
        print("\n")
        print(f"Stopping Server...")
        print(f"Thanks for using it! Have a great day 🤘👋\n")
        sys.exit(187)
