
### Trying to familiarize myself with Dockerfiles and best practices.
```
How to use:

1. Download Dockerfile
2. Create temp dir : "mkdir -p ~/temp/dockerfiles" (your choice)
3. cd to dir : "cd $!"
4. run build with tag (-t) option: "docker build -t <appname:tag> ."
```
example I use for httpie:
```
In the directory with the docker file (~/coding-interviews/misc/dockerfiles/apline/httpie/Dockerfile):
docker build -t http:latest .
```
> Written with [StackEdit](https://stackedit.io/).
