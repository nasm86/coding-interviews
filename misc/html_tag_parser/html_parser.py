#!/usr/bin/env python3
# func(html)
# => { "html" : 1 }
#def func(htmlString: str) -> Dict[str, int]:
# return {}

def gethtmltags():
    sttags      = {}
    html        = "<html></html><li></li><table></table><html></html><li></li><html></html><li></li><html></html><table></table>"
    test = html.split("<")[1:]
    for item in test:
        if '/' in item:
            pass
        else:
            blah = item.split(">")[0]
            if blah in sttags:
                sttags[blah] += 1
            else:
                sttags[blah] = 1
    return sttags
    
if __name__ == '__main__':
    try:
        wee = gethtmltags()
        wees = sorted(wee.items(), key=lambda x: x[1], reverse=True)
        for key, val in wees:
            print(f"{key} : {val}")
    finally:
        pass
