#!/usr/bin/python3

import crypt

def genhash(hashword):
    hashword    = crypt.crypt(hashword, crypt.mksalt())
    return hashword

if __name__ == '__main__':
    gpass   = input("enter string to hash: ")
    genhash = genhash(gpass)
    print(genhash)
