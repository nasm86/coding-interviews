#!/usr/bin/python3

def is_leap(year):
    leap = False
    if (year % 4) == 0:
        if (year % 100) == 0:
            if (year % 400) == 0:
                leap = True
            else:
                leap = False
        else:
            leap = True
    else:
        leap = False                 
    # Write your logic here
    
    return leap

if __name__ == '__main__':
    try:
        year = int(input("Year: "))
        print(is_leap(year))
    finally:
        pass

