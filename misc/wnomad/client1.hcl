log_level = "DEBUG"
data_dir = "/tmp/nomad/client1"
name = "wclient1"
client {
	enabled = true
	servers = ["127.0.0.1:4647"]
}

ports {
	http = 5656
}
