#Increase log level
log_level = "DEBUG"

#data dir
data_dir = "/tmp/nomad/server1"

#Enable
server {
	enabled = true
	bootstrap_expect = 1
}

