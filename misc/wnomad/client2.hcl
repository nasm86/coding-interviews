log_level = "DEBUG"
data_dir = "/tmp/nomad/client2"
name = "wclient2"
client {
	enabled = true
	servers = ["127.0.0.1:4647"]
}

ports {
	http = 5657
}
