provider "google" {
	project = "crucial-cycling-263516"
	region 	= "us-central1"
	zone	= "us-central1-c"
}

resource "google_compute_instance" "vm_instance" {
	name            = "terraform-nomad-instance"
	machine_type	= "f1-micro"

	boot_disk {
		initialize_params {
			image = "debian-cloud/debian-9"
		}
	}
	metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync"
	
	network_interface  {
		network = "${google_compute_network.vpc_network.self_link}"
		access_config {
		}
	}
}

resource "google_compute_network" "vpc_network" {
	name                    = "terraform-nomad-network"
	auto_create_subnetworks = "true"
}

output "ip" {
 value = "${google_compute_instance.vm_instance.network_interface.0.access_config.0.nat_ip}"
}

