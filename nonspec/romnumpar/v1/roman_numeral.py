#!/usr/bin/env python3

import os
import sys

def parseNumeral():
    rtokens     = {
        "I": "1",
        "V": "5",
        "X": "10",
        "L": "50",
        "C": "100",
        "D": "500",
        "M": "1000"
    }
    token       = input("Enter Character to check: ")
    tokup       = token.upper()

    if tokup in rtokens:
        print(f"{tokup} is a Roman numeral and it's value is {rtokens[tokup]}")
    else:
        print(f"{tokup} is not a Roman numeral")

if __name__ == '__main__':
    try:
        parseNumeral()
    finally:
        print(f"Thanks for using {os.path.basename(sys.argv[0])}\n")