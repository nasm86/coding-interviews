#!/usr/bin/env python3 

import hashlib
import os, os.path
import sys

if not sys.argv[1:]:
      udir = os.getcwd()
else:
      udir = sys.argv[1]

class DiskWalker(object):
    def __init__(self,path):
        self.path = path

    def enumpaths(self):
        path_collection = []
        for dirpath, dirnames, filenames in os.walk(self.path, followlinks=False):
            for file in filenames:
                full_path   = os.path.join(dirpath,file) 
                check_file  = os.path.isfile(full_path) 
                #added the check_file due to broken symlink fuckery
                if check_file:
                    path_collection.append(full_path)
        return path_collection

    def enumfiles(self):
        file_collection = []
        for dirpath, dirnames, filenames in os.walk(self.path, followlinks=False):
            for file in filenames:
                file_collection.append(file)
        return file_collection

def create_file_checksum(path):
    with open(path, 'rb') as file_pointer:
        checksum = hashlib.md5()
        while True:
            try:
                buffer = file_pointer.read(8192)
                if not buffer:break
                checksum.update(b'{buffer}')
                checksum = checksum.hexdigest()
            finally:
                pass
            return checksum

def find_duplicates(udir):
    duplicate   = []
    record      = {}
    walker      = DiskWalker(udir)
    path_files  = walker.enumpaths()
    for file in path_files:
        compound_key = (os.path.getsize(file), create_file_checksum(file))
        if compound_key in record:
            duplicate.append(file)
        else:
            record[compound_key] = file
    return duplicate

if __name__ == '__main__':
    try:
        dupes = find_duplicates(udir)
        if dupes: 
            print(f"{dupes}") 
        else: 
            pass
    finally:
        pass