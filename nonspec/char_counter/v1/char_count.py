#!/usr/bin/env python3

def countChar(word):
    letters   = {}
    count     = 0

    while count <= len(word):
        count += 1
        print(f"\nWord: {word}\n")

        for char in set(word):
            letters[char] = word.count(char)
        return letters

if __name__ == '__main__':
    try:
        count = countChar(input("\nPlease input strings: "))
        skeys = sorted(count.items(), key=lambda x: x[1], reverse=True)
        for k,v in skeys:
            if k == ' ':
                k = 'space'
            print(f"Char: {k} Count: {v}")
    finally:
        print("\nThanks for playing!\n")